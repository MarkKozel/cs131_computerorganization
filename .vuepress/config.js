module.exports = {
  title: "CS 131 - Computer Organization",
  description: "Transistors to Assembly - A bottom-up View of Computer Science",
  configureWebpack: {
    resolve: {
      alias: {
        "@chapters": "/docs/",
      },
    },
  },
  themeConfig: {
    sidebar: [
      {
        title: "Introduction", // required
        path: "/docs/intro/", // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 3, // optional, defaults to 1
        children: ["/docs/intro/"],
      },
      {
        title: "Section 1", // required
        path: "/docs/section1/", // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 3, // optional, defaults to 1
        children: [
          "/docs/section1/",
          "/docs/section1/1/",
          "/docs/section1/2/",
          "/docs/section1/3/",
        ],
      },
      {
        title: "Section 2", // required
        path: "/docs/section2/", // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 3, // optional, defaults to 1
        children: [
          "/docs/section2/",
          "/docs/section2/1_LC3/",
          "/docs/section2/2_ArithmeticOps/",
          "/docs/section3/9/",
          "/docs/section2/3_DataMoveOps/",
          "/docs/section2/4_FlowControlOps/",
        ],
      },
      {
        title: "Section 3", // required
        path: "/docs/section3/", // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 3, // optional, defaults to 1
        children: [
          "/docs/section3/",
          "/docs/section3/8/",
          "/docs/section3/10/",
        ],
      },
      {
        title: "Reference Materials", // required
        path: "/docs/zz_referenceMaterials/", // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 3, // optional, defaults to 1
        children: [
          "/docs/zz_referenceMaterials/",
          "/docs/zz_referenceMaterials/Definitions/",
        ],
      },
      {
        title: "Test Stuff", // required
        path: "/docs/test/", // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 3, // optional, defaults to 1
        children: ["/docs/test/", "/docs/test/Guide/"],
      },
    ],
    logo: "/assets/img/CourseLogo.png",
    lastUpdated: true,
  },

  markdown: {
    lineNumbers: true,
  },
  plugins: [
    "vuepress-plugin-copyright",
    {
      noCopy: true, // the selected text will be uncopiable
      minLength: 40, // if its length is greater than 100
      authorName:
        "Mark Kozel. This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License",
      clipboardComponent: "../components/ClipboardComponent.vue",
    },
    "@vuepress/back-to-top",

    [
      "vuepress-plugin-container",
      {
        type: "readmore",
        before: (info) =>
          `<div class="readmore">
            <p class="readmoretitle">Read More: <em> ${info}</em></p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "left",
        defaultTitle: "",
      },
    ],

    [
      "vuepress-plugin-container",
      {
        type: "right",
        defaultTitle: "",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "thinkaboutit",
        before: (info) =>
          `<div class="thinkaboutit"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "tip",
        defaultTitle: "Tip",
      },
    ],
  ],
  // dest: "public",
  base: "/cs131_computerorganization/",
};
