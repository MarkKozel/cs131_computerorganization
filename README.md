# Welcome to CS 131

## Computer Organization

![Course Logo](/assets/img/CourseLogo.png)

Transistors to Assembly - A bottom-up view of Computer Science
::: tip See More
Use the Navigation (to the left) to see more stuff
:::

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
