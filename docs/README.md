<!-- ---
home: true
heroImage: /assets/img/Sphere.png
actionText: LC-3 →
actionLink: /docs/LC3
features:
- title: Simplicity First
  details: Minimal setup with markdown-centered project structure helps you focus on writing
- title: Vue-Powered
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: Performant
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.

footer: Mark Kozel (MKozel@HancockCollege.edu) - Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
--- -->

# Welcome to CS 131 - Computer Organization

Transistors to Assembly - A bottom-up view of Computer Science

Use the Navigation (to the left) to see more stuff

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
