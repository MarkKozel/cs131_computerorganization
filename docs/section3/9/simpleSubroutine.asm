.ORIG x3000

main    AND R0, R0, #0  ; Clear R0
        ADD R0, R0, #2  ; Set 2 in R0
        JSR addOne      ; Call subroutine 'addOne'
        ; R1 contains R0 + 1

addOne  add R1, R0, #1
        RET

.END