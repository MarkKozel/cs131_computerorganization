# Chapter 9 - Subroutines and TRAP Rountines

The ability to write small, single-purpose blocks of code that can be used repeatably is a minimal requirement for modern programming languages. It is unlikely that new programming language would succeed today without methods/subroutines

Advantages of using subroutines include:

- Reuse - can be used many times in a program without needing to copy/paste code in multiple places in main program
- Testing - once a subroutine is tested, it can be

[[toc]]

## Save and Restore Registers

We have already stored a register into a memory location as part of assignments where the result of some calculation was to be saved in memory

There is another reason to store registers in memory...to save the state of your code when jumping to a TRAP Routine or a user-defined function. In both cases, R0-R7 are the only registers for the entire LC-3 Simulate environment. It another function uses a register your code was using, your data will be overwritten. Saving and restoring registers is essential to prevent data loss

<QuestionMC question="The LC-3 instruction for storing a register R4 to a memory location labeled 'Result' is..." answer='C' AChoice="STI R5, #12" BChoice="ST Result, R5" CChoice="ST R5, Result" DChoice="R5 .BLKW 1" rightAnswerFeedback="The simplest PC-relative Data Movement instruction" wrongAnswerFeedback="ST R5, Result, the PC-relative Data Movement instruction is the correct answer"/>

:::tip Who is responsible to saving and restoring registers
The caller code (your code) or the callee code (TRAP or function yoru code calls) can both be responsible for preventing register data loss

It would be a waste of clock-cycles and memory for both caller and callee to perform thsi work
The caller code could, but it would have to save all 8 registers becuase it does not know which registers the calle code will be using

It is more practice for the callee code to save only the registers it knows it will be using, then restore only those registers before returning to the caller code

In addition, R7 is modifed during the call to a TRAP or user-defined function, so the caller could never save it accuratly

Finally, the caller code is kept smaller by not having save/restore code included
:::

<QuestionTF question="It is the Callee's responsiblity to save registers that it changes" answer='true' rightAnswerFeedback="The callee is the only code that knows what it changes" wrongAnswerFeedback="The caller does not know what registers the callee will use"/>

### R7 - The way back home

During the instruction cycle for `TRAP`, `JSR`, and `JSRR`, R7 is set to the current PC (The next instruction in our caller code)

When `RET` is called by the callee code, the PC will be loaded with the address in R7, which will return the PC back to the caller's next instruction

The callee code is expected to save/restore R7 always. Should the TRAP/User Function call another TRAP/User Function, the way back to the original caller code will be lost

<QuestionTF question="R7 is not important, thus does not need to be saved" answer='false' rightAnswerFeedback="It's the 'way back home' to our code that called the subroutine" wrongAnswerFeedback="It is used by the RET instuction in the subroutine to return back to our calling code"/>

### Callee storing registers (Example)

The following Subroutine, _mySub_ will use R1 and R2 internally. As the Callee, it is resonsible for saving R1 and R2 before using those registers. Once complete, it will restore both registers, then return to tha Caller

```asm
mySub   ST R1, SaveR1 ;save R1
        ST R2, SaveR2 ;save R2
        ST R7, SaveR7 ;save R7

        ;code that changes R1 & R2
        AND R1, R1, #17
        ADD R2, R1, #-12

        LD R1, SaveR1 ;restore R1
        LD R2, SaveR2 ;restore R2
        LD R7, SaveR7 ;restore R7
        RET           ;Return back to caller with R1 & R2 restored

;Allocate 1 memory slots for storing registers
SaveR1  .BLKW 1
SaveR2  .BLKW 1
SaveR5  .BLKW 1
```

## Subroutines

Java example of a call to a user-defined method (subroutine)
In this example, main() defines a variable x, then passes **a copy** of it to the method _addOne()_. _addOne()_ does something to a copied variable, then passed the result back to the main program

```java
public static void main(String[] args){
    int x = 2;
    int y = addOne(x);
}

public int addOne(int value){
    return value + 1;
}
```

In LC-3, the code clears and sets R0. It then jumps to _addOne_ label (subroutine name). \*addOne code adds 1 to the value in R0, and stores it in R1. Control is returned to the main code for further execution

```asm
.ORIG x3000

main    AND R0, R0, #0  ; Clear R0
        ADD R0, R0, #2  ; Set 2 in R0
        JSR addOne      ; Call subroutine 'addOne'
        ; R1 contains R0 + 1

addOne  add R1, R0, #1
        RET

.END
```

:::tip Main difference between Java and LC-3 Subroutines
**Variable Name**

Java manages data/values for the code. Variable are created with user-defined names, then the remaining code references values using those names

LC-3's primary varaible stores are Registers (R0-R7). It is left to the programmer to remmember what each register contains and why

**Pass by Value**

Java makes a copy of primative data types (like _int_) when passing to a method

LC-3 only has 1 set of registers. Any change to a register in a subroutine remains when it returns

**Return Value**

Java methods return allows the calling code to capture the result in a seperate variable name

LC-3, only having 1 set of registers, forces the main and subroutine to pre-agree on which register contains the parameter and which register will contain the _return_ value
:::

### LC-3's Subroutine Opcodes

#### JSR

| OpCode  | Mode |      PCOffset11       |
| :-----: | :--: | :-------------------: |
| 0 1 0 1 |  1   | 0 0 0 0 0 0 0 0 0 0 0 |

**OpCode**: Bits [15:12]

**Mode**: Bits [11:11] (1 for JSR, 0 for JSRR)

**PCOffset11**: Bits [10:0] 11-bit offset PC-realtive jump [Range is -1024 to 1023]

#### JSRR

| OpCode  | Mode |     | BaseR |               |
| :-----: | :--: | :-: | :---: | ------------: |
| 0 1 0 1 |  1   | 0 0 | 0 0 0 | 0 0 0 0 0 0 0 |

**OpCode**: Bits [15:12]

**Mode**: Bits [11:11] (1 for JSR, 0 for JSRR)

**BaseR**: Bits [8:6] Register containing 16-bit address of subroutine starting point

#### RET

| OpCode  |       |  Reg  |             |
| :-----: | :---: | :---: | :---------: |
| 1 1 0 0 | 0 0 0 | 1 1 1 | 0 0 0 0 0 0 |

**OpCode**: Bits [15:12]

**Reg**: Register containing address to jump (return)to. Harcoded as R7

## TRAP Routines

The LC-3 provides a few built-in functions to make the system easier to use. These are common routines that typical programs might use

::: tip TRAP Routines in other Instruction Architecture Sets
TRAP routines are ISA-defined. Each ISA may provide routines to support the intended/expected use of the ISA. An ISA designed for communication might include TRAP functions to connect to external systems and support basic I/O functions
:::

<QuestionTF question="TRAP Routines are special code hidden in the LC-3's micro-code that we cannot see?" answer='false' rightAnswerFeedback="It's just plain LC-3 assembly instruction to make out lives a little easier" wrongAnswerFeedback="It is assembly code built in to the LC-3 simulate environment. Jump to memory location x0450 to look at the PUTS Trap code"/>

The LC-3, designed as a teaching ISA, includes TRAP functions to support general keyboard input and terminal output

| Address | Vector | Routine Name | Purpose                                                                                                                                                                              |
| :-----: | :----: | -----------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|  x0020  | x0400  |         GETC | Read one input character from the keyboard and store it into R0 _without echoing the character to the console_                                                                       |
|  x0021  | x0430  |          OUT | Output character in R0 to the console                                                                                                                                                |
|  x0022  | x0450  |         PUTS | Output null terminating string to the console starting at address contained in R0                                                                                                    |
|  x0023  | x04A0  |           IN | Read one input character from the keyboard and store it into R0 _and echo the character to the console_                                                                              |
|  x0024  | x04E0  |        PUTSP | Same as PUTS except that it outputs null terminated strings with two ASCII characters packed into a single memory location, with the low 8 bits outputted first then the high 8 bits |
|  x0025  | xFD70  |         HALT | Ends a user’s program                                                                                                                                                                |

> LC-3 Trap Vector Table

The LC-3 contains a vector (jump) table for TRAP functions in memory location x0020 - x0025. Each address in this jump table contains the location, in memory, of the start of a given TRAP function code block

<QuestionTF question="If the LC-3 creators wanted to move PUTS from x0450 to x0460, they would only need to change the LC-3 Trap Vector Table's Vector for PUTS?" answer='true' rightAnswerFeedback="That is the main advantage to Vector Tables...it is easy to relocate the code and update the Vector Table" wrongAnswerFeedback="The Vector Table serves as an address book for TRAP Routines. TRAP x22 will run whatever code is at the Vector entry for x0022"/>

::: tip How the LC-3 Executes a TRAP PUTS instruction
During the execution the `TRAP PUTS` instruction, the LC-3 controller 'knows' the PUTS vertor entry in the table is memory location x0022. The controller requested the contents of memory location x0022, and receives the address x0450

The controller stores the current PC (instruciton after the `TRAP PUTS` instruction) in R7, and changes the PC to x0450. It then ends the instruction cycle

The next instruction cycle starts with the PC x0450, and the first line of the PUTS instruction begines a new instruction cycle
:::

Things to note:

- The Jump Table address (x0022) for PUTS contains the address (x0450) of the first line of code for the `PUTS TRAP` function
- Before the PC is changed to x0450, the LC-3 controller stashed the current PC in R7
  - Remember that R7 is used by `RET` to return back to the

### The PUTS Function

```{.line-numbers}
.ORIG x0450
	;;; Save Registers ;;;
SavRegs	ST R7, R7_Store ; Return Address
		ST R0, R0_Store ; Pointer to String to Puts
		ST R1, R1_Store
		ST R2, R2_Store

Loop	LDR R1, R0, #0; Get address of first char of string
		BRz Done ;	Char was 0, so we are done

Wait	LDI R2, DSR ;	Console available?
		BRzp Wait 	;	No, loop back and try again

		STI R1, DDR ;	Write char to console

		ADD R0, R0, #1;	Move pointer to next char in String
		BRnzp Loop ;	Loop back for next caracter

	;;; Restore Registers ;;;
Done	LD R0, R0_Store
		LD R1, R1_Store
		LD R2, R2_Store
		LD R7, R7_Store

		RET

;Data Declarations-------------
	DSR		.FILL xFE04
	DDR		.FILL xFE06
	MX0462		.FILL xF3FD
	MX0463		.FILL xF3FE
	R0_Store	.FILL #0
	R1_Store	.FILL #0
	R2_Store	.FILL #0
	R7_Store	.FILL #0
.END
```

Things to note:

- PUTS first stores the current contents of R7, R0, R1, R2 in memory
  - R7 contains the location of the calling code's next instruciton
  - R0 was set in the calling code just before calling PUTS. It contains the addres of the first charcter to be output by PUTS
    - PUTS stores R0 at the start becuase it will chnage R0 as it prints each character
    - The calling code may expect R0 to be set like it was before it called PUTS
  - R1 and R2 are used in the execution of PUTS. Because PUTS has no way of knowing if the calling code was using these registers, it stores them to be safe
- Next PUTS gets the first character, stored in R0 by the calling code
  - At any time R0 contains zero (0), PUTS will exit this loop as all characters have been written to the console
  - a value zero (0) is a special case to PUTS. It is the end-of-string marker
- Expecting that R0 does not contain zero (0), PUTS waits for the console to be ready, then copies the value in R0 (an ascii number representing the character to display) to the Display Data Register
- R0 is incemented to point to the next character in the string to display
- PUTS unconditionally loop back to check for zero (0), and start the cycle again
- Once R0 is loaded with zero (0), PUTS is done displating characters
  - R7, R0, R1, and R2 original values are copied from memory back to each register
- `RET` is called, and the PC is loaded with the value n R7 (the calling code's next instruction)
- Execution continues ate the calling code's next instruciton after PUTS was called

:::tip You Do It - Find and review GETC

1. Start the LC-3 Simulate environment. Don't load an code, just use the system are it starts up
2. Jump to memory address x0020

![Jump to X0020](./TRAP_JumpTox20.png)
Enter x0020 in Jump to box and press enter

3. Note the value in x0020 It should be x0400
4. Jump to memory address x0400
5. Play LC-3 Computer and trace the execution of GETC
6. Answer the followin: questions:

- What register(s) are saved initially and restored before RET?
- What Data Declarations are present after RET? What special memory addresses are these?
- What are the 2 uses of R0? Why is it not saved and restored in GETC?
  :::

###
