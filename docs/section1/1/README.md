# Chapters 1

## Abstraction

A model of a system with a hierarchy based on details. Each layer defined the previous layer in more detail

Read more about [Abstraction](./1_abstraction.md)

## Analog and Digital Computers

Humans developed devices to complete tasks (everyday and labor-intensive) 1) faster, and 2) reliably. As new materials and technologies become available (and cost-effective), these devices are upgraded or replaced.

Read more about [Analog and Digital Computers](./2_AnalogDigitalComputers.md)

## Specific- and General-Purpose Computers

Read more about [Specific- and General-Purpose Computers](./3_SpecificGeneralUseComputers.md)

## Turing Machines

Read more about [Turing Machines](./4_TuringMachines.md)