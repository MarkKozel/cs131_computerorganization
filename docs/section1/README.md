# Section 1
This section will define the concepts and operations used in modern computer systems

## [Chapter 1](./1/README.md)
### Abstraction and the Hardware/Software Connection
* Abstraction, as it applies to Computer Organization
* Analog and Digital Computers
* General Purpose Computers
* Turing Machines

## [Chapter 2](./2/README.md)
### The Computer Number System and Logic Operations
* Base 2 (Binary) Number System
    * Converting to Base 10 (Decimal)
    * Binary Arithmetic
    * Base 16 (Hexadecimal) and Base 8 (Octal) Number Systems
    * Representing Numbers and Data in Binary
* Binary Operations
    * Logical Operations
        * NOT, AND, OR
        * XOR, NAND, NOR

## [Chapter 3](./3/README.md)
### Electrons to Bits
* Devices
    * Transistors
    * Voltage to Binary Conversion
* Circuits
    * Logic Gates
    * Control Circuits
        * Decoder, Multiplexer
    * Adder Circuit
    * State and Storage Circuits
* Micro Architecture
    * Control
    * Storage
    * Data Manipulation
    * Data Bus