# Examples Section

## Links to other .md files
```md
[one.md](./one.md)
```
[one.md](./one.md)

[two.md](./two.md)

## Images
```
<img src="../section1/1/resources/2000px-Biological_classification_L_Pengo_vflip.svg.png" width="10%" height="10%">
```
<img src="../section1/1/resources/2000px-Biological_classification_L_Pengo_vflip.svg.png" width="10%" height="10%">

## Text and Image in Table
```html
<table>
    <tr>
        <td style="width:80%" >Abstraction in Biology </td>
        <td style="width:20%"><img src="../section1/1/resources/2000px-Biological_classification_L_Pengo_vflip.svg.png" style="max-height:100%; max-width:100%"/> </td>
    </tr>
</table>
```
<table>
    <tr>
        <td style="width:80%" >Abstraction in Biology </td>
        <td style="width:20%"><img src="../section1/1/resources/2000px-Biological_classification_L_Pengo_vflip.svg.png" style="max-height:100%; max-width:100%"/> </td>
    </tr>
</table>

## Custom VUE Components
vue files are in .vuepress\component folder

### True/False Inline Questions
vue files are in .vuepress\component folder

```vue
<QuestionTF question="C's get Degrees" answer='true' rightAnswerFeedback="Technically true, but Bs and As are still better" wrongAnswerFeedback="Although technically incorrect, shooting higher is bound to be better"/>
```

<QuestionTF question="C's get Degrees" answer='true' rightAnswerFeedback="Technically true, but Bs and As are still better" wrongAnswerFeedback="Although technically incorrect, shooting higher is bound to be better"/>


### Multiple Choice Inline Question

```vue
<QuestionMC question="The best way to get past a problem is..." answer='C' AChoice="Ignore it" BChoice="Deny as long as possible" CChoice="Address is directly" DChoice="Hope tomorrow is better" rightAnswerFeedback="Right! It make make others uncomfortable, but this is better in the long term" wrongAnswerFeedback="Bandaid Solution!"/>
```

<QuestionMC question="The best way to get past a problem is..." answer='C' AChoice="Ignore it" BChoice="Deny as long as possible" CChoice="Address is directly" DChoice="Hope tomorrow is better" rightAnswerFeedback="Right! It make make others uncomfortable, but this is better in the long term" wrongAnswerFeedback="Bandaid Solution!"/>

### You Do It activity header

```vue
<YouDoIt time=12 instructions="follow the white rabbit"/>
```
<YouDoIt time=12 instructions="follow the white rabbit"/>


### Key Concepts
```vue
<KeyConcepts :ConceptArray= "[
{
  Concept:'Natural Language',
  Details:'Spoken and written communication developed and evolved over time, and used by humans'
},
{
  Concept:'Ambiguity',
  Details:'Imprecise nature of a construct. For Natural Language, how individuals interprets the meaning or intent of a word or phrase'
},
{
  Concept:'Algorithm',
  Details:'In the context of Computer Science, re-writing a Problem in a form that minimizes the Ambiguity of Human/Natural Language'
}
]" />
```
<KeyConcepts :ConceptArray= "[
{
  Concept:'Natural Language',
  Details:'Spoken and written communication developed and evolved over time, and used by humans'
},
{
  Concept:'Ambiguity',
  Details:'Imprecise nature of a construct. For Natural Language, how individuals interprets the meaning or intent of a word or phrase'
},
{
  Concept:'Algorithm',
  Details:'In the context of Computer Science, re-writing a Problem in a form that minimizes the Ambiguity of Human/Natural Language'
}
]" />


## vuepress-plugin-container elements
see .vuepress/config.js
styles found in .vuepress/styles/index.styl

see [Plugin Page](https://vuepress.github.io/en/plugins/container)

### Read More
```md
::: readmore Abstraction Layers
[Abstraction Layers in Computer Architecture](https://en.wikipedia.org/wiki/Abstraction_layer#Computer_architecture)
High-level abstraction of computer architecture
:::
```
::: readmore Abstraction Layers
[Abstraction Layers in Computer Architecture](https://en.wikipedia.org/wiki/Abstraction_layer#Computer_architecture)
High-level abstraction of computer architecture
:::

### Think About It
```md
::: thinkaboutit Newton's First Law
In an inertial frame of reference, an object either remains at rest or continues to move at a constant velocity, unless acted upon by a force.
```
::: thinkaboutit Newton's First Law
In an inertial frame of reference, an object either remains at rest or continues to move at a constant velocity, unless acted upon by a force.
:::

### Right container (with link)
```md
::: right
From [Wikipedia](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion)
:::
```
::: right
From [Wikipedia](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion)
:::

### Tip
```md
::: tip
Tip container of `@vuepress/theme-default`
:::
```
::: tip
Tip container of `@vuepress/theme-default`
:::