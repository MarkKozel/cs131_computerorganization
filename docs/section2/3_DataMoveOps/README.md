# Section 2-3 Data Move Ops

Once we have used data operations on our data types, our program may need to store results for use elsewhere in the program. Data Move Operations provide instructions to store our data and retrive it from memory
