//***Primitive Types***//
//Logic
Boolean isDone = false;

//Character
char myGrade='A'; //16 bits, unicode

//Integers (2's Complement Value)
byte driversLicenseAge = 16; //8 bits
short decadeLength = 10; //16 bits
int desiredAnnualSalary = 12345678; //32 bits
long kmToMars = 54600000; //64 bits

//Floating Point
float gpa = 3.84; //32-bit single-precision IEEE 754 floating point
double massOfSun = 1.989e30;//64-bit double-precision IEEE 754 floating point

//***Non-Primiative Types***//
String name = new String ("Steve Martin"); //16 bits (char) x 12 (# of chars in array)
int[] testScores = {98,87,92,76}; //32 bits (int) x 4 (# of elements in array)
//and many more