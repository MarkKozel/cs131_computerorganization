# Section 2-2 Data Type/Ops

Programming languages manipulate data using functions.

All programming languages have functions/operations that change information. Depending on the language, these operations have be simple or complex, depending on the purpose of the language

As an example, Java contains

<<< docs/section2/2_ArithmeticOps/javaDataTypes.java

<QuestionMC question="What do all these java types have in common?" answer='C' AChoice="Variable names are too long" BChoice="All the same size in Java" CChoice="They all have fixed bit lengths" DChoice="The variable name letter case is weird" rightAnswerFeedback="Yes. bits remain the universal unit of measure for modern computers...for now (Google 'Quantum Computers' to see learn how that is going to change)" wrongAnswerFeedback="Nope, try again"/>
