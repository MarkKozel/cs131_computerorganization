# LC-3 Getting Started

## Download the Tools <Badge text="Version 3.01" type="tip"/>
::: warning Get Version 3.01
There are a few different versions out there. To ensure we are all using the same version, download as described below
:::
[McGraw Hill LC-3 Simulator](https://highered.mheducation.com/sites/0072467509/student_view0/lc-3_simulator.html)
Download the Windows or Unix version
Run the downloaded executable, then select the directory to install the tools

## The LC-3 Tools
### LC3Edit
The LC3 editor is used to create, modify, and store LC-3 assembly source code. The editor also contains the assembler tools to generate LC-3 assembly object code
![LC3 Edit Main Window](/assets/img/LC3/LC3EditMain.png)

### Simulate
Once LC-3 assembly source code is assembled into object code, it is loaded into the Simulate environment. The simulator is a virtual LC-3 Microarchitecture that will execute object code. The environment shows the state of general registers, program and instruction registers, condition code, and contents of all RAM.

It also provides a debugger to step through object code 1 line at a time
![LC3 Simulate Main Window](/assets/img/LC3/LC3SimulateMain.png)
::: tip
Debugging is a seperate tutorial
:::

## Using the Tools